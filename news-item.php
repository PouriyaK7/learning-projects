<?php
require '__include/lib.php';

if (isset($_GET['q'])) {
    $stmt = Lib::DB()->prepare('SELECT * FROM `news` WHERE `slug` = ?');
    $stmt->bind_param('s', $_GET['q']);
    $stmt->execute();
    $item = $stmt->get_result()->fetch_assoc();
    $result = "
            <div class=\"post-wrapper\">
            <div class=\"post-img\" style=\"background-image: url('cdn/img/Circle-PK.jpg')\">
                 
            </div>
            <h3>
                 ".$item['title']."
            </h3>
            <span>
             ". $item['date']."
        </span>
            <p>
                ". $item['text']."
            </p>
       
           
        </div>
            ";
    echo $result;
}
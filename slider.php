<?php
require '__include/lib.php';
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Slider</title>
    <link rel="stylesheet" href="cdn/css/style.css">
    <style>
        #slider-text-change {
            -webkit-animation: fadein 1.1s ease-in-out; /* Safari, Chrome and Opera > 12.1 */
            -moz-animation: fadein 1.1s ease-in-out; /* Firefox < 16 */
            -o-animation: fadein 1.1s ease-in-out; /* Opera < 12.1 */
            animation: fadein 1.1s ease-in-out;

            font-size: 25px;
            margin-bottom: 20px;
        }

        @keyframes fadein {
            from {
                opacity: 0;
                padding-top: 100px;
            }
            to {
                opacity: 1;
                padding-top: 0;
            }
        }
    </style>
</head>
<body>

<div style="display: table;width: 100%;height: 100vh;background-color: red">
    <div style="display: table-row">
        <div id="slider-text-change" style="display: table-cell;vertical-align: middle;text-align: center">
             <span>
                Welcome to my website
            </span>
        </div>
    </div>
</div>

<script src="cdn/js/main.js"></script>
<script src="cdn/js/me.js"></script>
<script src="cdn/js/modals.js"></script>
</body>
</html>

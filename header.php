<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Project 1</title>
    <link rel="stylesheet" href="cdn/css/style.css">
</head>
<body id="body">
<div id="sidebar-shadow">

</div>
<div id="sidebar" class="sidebar">
    <a id="sidebar-closeBtn" onclick="sidebar_close()">&#9747;</a>
    <a id="home" href="/" class="active">Dashboard</a>
    <a href="/karomands.php" id="karomands">Karomands</a>
    <a id="login" style="<?php if (isset($_SESSION['id'])) echo 'display:none'; ?>">Login</a>
    <a id="register" style="<?php if (isset($_SESSION['id'])) echo 'display:none'; ?>">Sign-up</a>
    <a id="logout" onclick="logout()" style="<?php if (!isset($_SESSION['id'])) echo 'display:none'; ?>">Logout</a>
</div>
<div class="content" id="content">
    <?php if (!isset($hamburger_menu)) echo '<button id="hamburger-menu" class="btn hamburger-menu" onclick="sidebar_open()">&#9776;</button>'; ?>
-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2020 at 02:50 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project01`
--

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) NOT NULL,
  `title` varchar(220) COLLATE utf8_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `text` text COLLATE utf8_unicode_520_ci NOT NULL,
  `slug` varchar(250) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `date`, `text`, `slug`) VALUES
(1, 'My new learning project', '2020-01-11 16:35:30', 'I started learning css, js and html so i started this project', 'my-new-learning-project-1'),
(2, 'Hello world!', '2020-01-11 16:36:14', 'I didn\'t have any ideas for the next post, so here we are!', 'hello-world-2');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_520_ci NOT NULL,
  `username` varchar(250) COLLATE utf8_unicode_520_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_520_ci NOT NULL,
  `phone` varchar(250) COLLATE utf8_unicode_520_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `phone`, `password`) VALUES
(1, 'Pouriya Khazaei', 'pouriyak7', 'kpouriya6@gmail.com', '09228812579', ''),
(2, 'admin', 'admin13', 'admin@gmail.com', '13245678921', '$2y$10$9C/IHYk.5uu6VoAABbb3D.y2UhtDRwc5t9dDsIamlO7u61C3hKDNS'),
(3, 'admin', 'admin56', 'adminadmin@gmail.com', '12345679856', '$2y$10$SE6PgCGb/PpzMGuerlnyVuf3WrBnjAcf0T4TU9e.Zk8QOSD7JQdOK'),
(5, 'admin', 'admin569', 'adminadm1in@gmail.com', '12345679858', '$2y$10$oH8t24GbjiYJ9pZwuJJds.G0fon4/CFGsspVSG6GjoMaTs01CR21u'),
(7, 'admin', 'admin5691', 'adminadm21in@gmail.com', '09336654521', '$2y$10$pRSfS2IReTw1q2QVTTZUhuvJH2DGlwhMXUR49jYJAKsgGPOaYJjnG'),
(8, 'admin', 'admin5692', 'adminadm31in@gmail.com', '09336654524', '$2y$10$qdNxexsa8KGrmTkOFpQzt.Sjo2c8wcGP6ph/1Kwx7aJl6Up1.Euy6'),
(9, 'admin', 'admin5696', 'adminadm71in@gmail.com', '09336654584', '$2y$10$JhKZWDpQbUCyMzARBAy6vulmyujyVKNJVow4xzlQn2pE5xzpueDfu'),
(10, 'jake', 'himyme', 'jake@gmail.com', '09256365421', '$2y$10$.REtCbbFplKmTA/Brqbuj...TzE5tdtMw.44NxY8vJ9yVCecmh4fm'),
(11, 'Pouriya', 'learn', 'pk74ever@gmail.com', '09350539376', '$2y$10$atTBlFOHTRQXIAdavrT8yupDw9JqGlOJsrqo0Bi1xjRBsIDWzPaF6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

if (!defined('DB_HOST')) {
    require 'config.php';
}

if (empty($_SESSION)) {
    session_start();
}

class Lib
{
    public static $DB;

    public static function DB() : mysqli {
        self::$DB = new mysqli(DB_HOST, DB_USER, DB_PASS, DB__NAME);
        self::$DB->set_charset('utf8');

        return self::$DB;
    }

    public static function register($name, $username, $phone, $email, $password) {
        $name = trim($name);
        $phone = trim($phone);
        $email = trim($email);
        if (preg_match('/^[a-z0-9_.-]{5,20}$/i', $username) && $phone[0] == '0' && $phone[1] == '9' && strlen($name) > 3 && strlen($phone) == 11 && strlen($password) > 7 && strlen($password) < 17 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $password = password_hash($password, PASSWORD_DEFAULT);
            $username = strtolower($username);
            $stmt = Lib::DB()->prepare('INSERT INTO `users`(`name`, `username`, `phone`, `email`, `password`) VALUES (?,?,?,?,?)');
            $stmt->bind_param('sssss', $name, $username, $phone, $email, $password);
            if ($stmt->execute()) {
                Lib::put_session($stmt->insert_id, $name, $username, $phone, $email);
                return 1;
            }
        }
        return 0;
    }

    public static function login($username, $password) {
        $username = strtolower($username);
        $stmt = Lib::DB()->prepare('SELECT * FROM `users` WHERE `username` = ? OR `email` = ? OR `phone` = ?');
        $stmt->bind_param('sss', $username,$username,$username);
        $stmt->execute();
        $users = $stmt->get_result();
        $user = $users->fetch_assoc();
        if ($users->num_rows == 1 && password_verify($password, $user['password'])) {
            Lib::put_session($user['id'], $user['name'], $user['username'], $user['phone'], $user['email']);
            return 1;
        }
        return 0;
    }

    public static function logout() {
        session_destroy();
    }

    public static function put_session($id, $name, $username, $phone, $email) {
        $_SESSION = [
            'id' => $id,
            'name' => $name,
            'username' => $username,
            'phone' => $phone,
            'email' => $email
        ];
    }

    public static function homepage() : mysqli_result {
        $stmt = Lib::DB()->prepare('SELECT * FROM `news`');
        $stmt->execute();
        return $stmt->get_result();
    }

    public static function search_news($search) {
        $search = Lib::DB()->query("SELECT * FROM `news` WHERE `title` LIKE '%".$search."%' OR `text` LIKE '%".$search."%' GROUP BY `id`");
        $result = '';
        while ($item = $search->fetch_assoc()) {
            $result .= "
            <div class=\"post-wrapper\">
            <div class=\"post-img\" style=\"background-image: url('cdn/img/Circle-PK.jpg')\">
                 
            </div>
            <h3>
                 ".$item['title']."
            </h3>
            <span>
             ". $item['date']."
        </span>
            <p>
                ". $item['text']."
            </p>
            <span id='read-more' class=\"read-more\" onclick='readMore(\"".$item['slug']."\")'>
            Read more >>
        </span>
           
        </div>
            ";
        }
    return $result;
    }

    public static function add_post($title, $text) {
        $title = trim($title);
        $text = trim($text);
        $slug_title = strtolower($title);
        $last_id = Lib::DB()->query('SELECT MAX(`id`) AS my_id FROM `news`');
        $slug = str_replace(' ', '-', $slug_title) . '-' . ($last_id->fetch_assoc()['my_id'] + 1);

        // TODO check this!
        $stmt = Lib::DB()->prepare('INSERT INTO `news`(`title`, `text`, `slug`, `author`) VALUES (?,?,?,?)');
        $stmt->bind_param('sssi', $title, $text, $slug, $_SESSION['id']);
        return $stmt->execute()? 1: 0;
    }

    public static function edit_post($title, $text, $post) {
        $stmt = Lib::DB()->prepare('SELECT * FROM `news` WHERE `id` = ? AND `author` = ?');
        $stmt->bind_param('ii', $post, $_SESSION['id']);
        $stmt->execute();
        $stmt->get_result();
        $title = trim($title);
        $text = trim($text);
        if ($stmt->num_rows == 1) {
            $stmt = Lib::DB()->prepare('UPDATE `news` SET `title` = ?, `text` = ? WHERE `id` = ?');
            $stmt->bind_param('ssi', $title, $text, $post);
            $stmt->execute();
            $stmt->get_result();
            return $stmt->affected_rows == 1? 1: 0;
        }

        return 0;
    }

    public static function delete_post($post) {
        $stmt = Lib::DB()->prepare('DELETE FROM `news` WHERE `id` = ? AND `author` = ?');
        $stmt->bind_param('ii', $post, $_SESSION['id']);
        $stmt->execute();
        $stmt->get_result();
        return $stmt->affected_rows == 1? 1: 0;
    }

    public static function karomands_page($page = null) : mysqli_result {
        $stmt = Lib::DB()->prepare('SELECT * FROM `karomands`');
        $stmt->execute();
        return $stmt->get_result();
    }

    public static function karomand_search($karomand_name) {
        return Lib::DB()->query("SELECT * FROM `karomands` WHERE `karomand_name` LIKE '%".$karomand_name."%'");
    }

    public static function karomand_item($karomand_id) : mysqli_result {
        $stmt = Lib::DB()->prepare('SELECT * FROM `karomands` WHERE `karomand_id` = ?');
        $stmt->bind_param('i', $karomand_id);
        $stmt->execute();
        return $stmt->get_result();
    }
}
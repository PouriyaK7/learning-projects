<?php
require '__include/lib.php';
require 'header.php';

$news = Lib::homepage();
?>
<div class="content-header">
    <span id="isLogin"><?php echo !isset($_SESSION['id'])? 'Please Login': $_SESSION['name']; ?></span>
    <h2 id="content-header-title">
        My learning project
    </h2>
    <p id="content-header-subtitle">
        This is a project for learning html, css and js
    </p>
    <label>
        <input type="text" id="search" name="news-letter">
        <input type="button" class="btn" value="Search" onclick="search()">
    </label>
</div>

<div class="container" id="news-container">
    <?php while ($item = $news->fetch_assoc()) { ?>
        <div class="post-wrapper">
            <div class="post-img" style="background-image: url('<?php if (file_exists($item['slug'])) echo $item['slug']; else echo 'cdn/img/Circle-PK.jpg'; ?>')">
                 
            </div>
            <h3>
                <?php echo $item['title']; ?>
            </h3>
            <span>
            <?php echo $item['date']; ?>
        </span>
            <p>
                <?php echo $item['text']; ?>
            </p>
            <span id="read-more" onclick="readMore('<?php echo $item['slug']; ?>')">
            Read more >>
        </span>
        </div>
    <?php } ?>
</div>


<div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <span class="close" id="closeBtn">&times;</span>
                <h2>Modal Header</h2>
            </div>
            <div class="modal-body">
                <div id="login-content">
                    <h2 style="text-align: center">
                        Login form
                    </h2>
                    <form autocomplete="off">
                        <label>
                            <input id="username" type="text" name="username" placeholder="Username, E-mail or phone number">
                        </label>
                        <label>
                            <input type="password" id="password" name="password" placeholder="password">
                        </label>
                        <label style="text-align: center">
                            <input type="button" id="login-submit" value="Login" class="btn" style="display: block;margin: auto" onclick="login()">
                        </label>
                    </form>
                </div>
                <div id="register-content">
                    <h2 style="text-align: center">
                        Sign-up form
                    </h2>
                    <form autocomplete="off">
                        <label>
                            <input type="text" name="username" id="register-username" placeholder="username">
                        </label>
                        <label>
                            <input type="text" name="name" id="name" placeholder="name">
                        </label>
                        <label>
                            <input type="text" name="email" id="email" placeholder="email">
                        </label>
                        <label>
                            <input type="text" name="phone" id="phone" placeholder="phone number">
                        </label>
                        <label>
                            <input type="password" name="password" id="register-password" placeholder="password">
                        </label>
                        <label>
                            <input type="button" id="register-submit" value="Register" class="btn" style="display: block;margin: auto" onclick="register()">
                        </label>
                    </form>
                </div>
                <div id="news-content">

                </div>
            </div>
<!--            <div class="modal-footer">-->
<!--                <h3>Modal Footer</h3>-->
<!--            </div>-->
        </div>

</div>
    <script>
        document.getElementById('register-username').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                document.getElementById('register-submit').click();
            }
        });
        document.getElementById('name').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });
        document.getElementById('email').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });
        document.getElementById('phone').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });
        document.getElementById('register-password').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                login()
            }
        });
        document.getElementById('username').addEventListener('keyup', function (event) {
            if(event.keyCode == 13) {
                login();
            }
        });

        document.getElementById('password').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });

        document.getElementById('search').addEventListener('keyup', function (event) {
            if (event.keyCode == 13) {
                search()
            }
        })
    </script>
<?php
require 'footer.php';
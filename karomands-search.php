<?php
require '__include/lib.php';

if (isset($_GET['q'])) {
    $karomands = Lib::karomand_search($_GET['q']);
    if ($karomands->num_rows != 0){
        while ($item = $karomands->fetch_assoc()) {
            ?>
            <div class="post-wrapper">
                <div class="post-img" style="background-image: url('<?php  echo 'cdn/img/Circle-PK.jpg'; ?>')">
                     
                </div>
                <h3>
                    <?php echo $item['karomand_name']; ?>
                </h3>
                <span>
            <?php echo $item['karomand_school']; ?>
        </span>
                <p>
                    <?php echo $item['karomand_description']; ?>
                </p>
                <span id="read-more" onclick="karomandReadMore('<?php echo $item['karomand_id']; ?>')">
            Read more >>
        </span>
            </div>
            <?php
        }
    }
}
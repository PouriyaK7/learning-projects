<?php
require '__include/lib.php';

if (isset($_GET['q'])) {
    $karomands =  Lib::karomand_item($_GET['q']);
    if ($item = $karomands->fetch_assoc()) {
        ?>
        <div class="post-wrapper">
            <div class="post-img" style="background-image: url('<?php  echo 'cdn/img/Circle-PK.jpg'; ?>')">
                 
            </div>
            <h3>
                <?php echo $item['karomand_name']; ?>
            </h3>
            <span>
            <?php echo $item['karomand_school']; ?>
        </span>
            <p>
                <?php echo $item['karomand_description']; ?>
            </p>    
        </div>
        <?php
    }
}
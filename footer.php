<footer id="site-footer">
    <div id="footer-left">
        <h1>
            Created by IDK
        </h1>
    </div>
    <div id="footer-right">
        <a href="https://karomad.com">
            <h1>
                Karomad
            </h1>
        </a>
        <h1 id="footer-sep">
            |
        </h1>
        <a href="http://irib.ir">
            <h1>
                IRIB :|
            </h1>
        </a>
    </div>
</footer>
</div>
<script src="cdn/js/main.js"></script>
<script src="cdn/js/modals.js"></script>
<script src="cdn/js/me.js"></script>
</body>
</html>

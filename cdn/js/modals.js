var loginBtn = document.getElementById('login');

var registerBtn = document.getElementById('register');

var modal = document.getElementById('myModal');

var closeBtn = document.getElementById('closeBtn');

var loginContent = document.getElementById('login-content');

var registerContent = document.getElementById('register-content');

var newsContent = document.getElementById('news-content');

var karomandsContent = document.getElementById('karomand-overview-modal');

loginBtn.onclick = function () {
    modal.style.display = 'block';
    loginContent.style.display = 'block';
};

registerBtn.onclick = function () {
    modal.style.display = 'block';
    registerContent.style.display = 'block';
};

closeBtn.onclick = function () {
    modal.style.display = 'none';
    karomandsContent.style.display = 'none';
    loginContent.style.display = 'none';
    registerContent.style.display = 'none';
    newsContent.style.display = 'none';
};

window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = 'none';
        loginContent.style.display = 'none';
        registerContent.style.display = 'none';
        karomandsContent.style.display = 'none';
        newsContent.style.display = 'none';
    }
};
function logout() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (this.status = 200 && this.readyState == 4) {
            if (this.responseText != '') {
                document.getElementById('isLogin').innerHTML = 'Please Login';
                document.getElementById('login').click();
                document.getElementById('logout').style.display = 'none';
                document.getElementById('register').style.display = 'block';
                document.getElementById('login').style.display = 'block';
            }
            else {
                alert('login please');
                document.getElementById('login').click();
            }
        }
    };

    xmlHttp.open('GET', 'logout.php');
    xmlHttp.send();
}

function register() {
    var username = document.getElementById('register-username');
    var email = document.getElementById('email');
    var phone = document.getElementById('phone');
    var password = document.getElementById('register-password');
    var hi = document.getElementById('name');
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            if (this.responseText != '') {
                alert('register successful');
                document.getElementById('isLogin').innerHTML = this.responseText;
                document.getElementById('closeBtn').click();
                document.getElementById('login').style.display = 'none';
                document.getElementById('register').style.display = 'none';
                if (document.getElementById('sidebar-shadow').clientWidth != 0) {
                    sidebar_close();
                }
                document.getElementById('logout').style.display = 'block';
            }
            else {
                alert('Error')
            }
        }
    };
    xmlHttp.open('GET', 'register.php?username=' + username.value + '&name=' + hi.value + '&password=' + password.value
        + '&email=' + email.value + '&phone=' + phone.value);
    xmlHttp.send();
}

function login() {
    var loginUsername = document.getElementById('username');
    var loginPassword = document.getElementById('password');
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if(this.readyState == 4 && this.status == 200) {
            if (this.responseText != '') {
                alert('Login successful');
                document.getElementById('isLogin').innerHTML = this.responseText;
                document.getElementById('closeBtn').click();
                document.getElementById('login').style.display = 'none';
                document.getElementById('register').style.display = 'none';
                document.getElementById('logout').style.display = 'block';
                if (document.getElementById('sidebar-shadow').clientWidth != 0) {
                    sidebar_close()
                }
            }
            else {
                alert('Error')
            }
        }
    };
    xmlHttp.open('GET', 'login.php?username=' + loginUsername.value + '&password=' + loginPassword.value);
    xmlHttp.send();
}

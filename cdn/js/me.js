function search() {
    var search = document.getElementById('search');
    var container = document.getElementById('news-container');
    var xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = function () {
        if (this.readyState = 4 && this.status == 200) {
            if (this.responseText != '') {
                container.innerHTML = this.responseText;
            }
            else {
                container.innerHTML = 'Nothing to show';
            }
        }
    };
    xmlHttp.open('GET', 'search.php?search=' + search.value);
    xmlHttp.send();
}

function readMore(slug) {
    var xmlHttp = new XMLHttpRequest();
    var content = document.getElementById('news-content');

    xmlHttp.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            if (this.responseText != '') {

                content.innerHTML = this.responseText;
                document.getElementById('myModal').style.display = 'block';
                content.style.display = 'block';
            }
        }
    };

    xmlHttp.open('GET', 'news-item.php?q=' + slug);
    xmlHttp.send();
}

function karomandReadMore(id) {
    var xmlHttp = new XMLHttpRequest();
    const content = document.getElementById('karomand-overview-modal');

    xmlHttp.onreadystatechange = function () {
        if (this.status == 200 && this.readyState == 4) {
            if (this.responseText != '') {
                content.innerHTML = this.responseText;
                document.getElementById('myModal').style.display = 'block';
                content.style.display = 'block';
            }
        }
    };
    xmlHttp.open('GET', 'karomand-item.php?q=' + id);
    xmlHttp.send();
}
// sidebar const
const content = document.getElementById('content');
const hamburger = document.getElementById('hamburger-menu');
const sidebar = document.getElementById('sidebar');
const sidebarShadow = document.getElementById('sidebar-shadow');
sidebarShadow.style.display = 'none';
sidebarShadow.style.width = '100%';

function sidebar_open() {
    sidebar.style.width = '50%';
    sidebar.style.display = 'block';
    hamburger.style.display = 'none';
    document.getElementById('sidebar-closeBtn').style.content = '';
    sidebarShadow.style.display = 'block';
}

function sidebar_close() {
    document.getElementById('sidebar-closeBtn').style.content = '';
    sidebar.style.width = '0';
    hamburger.style.display = 'block';
    sidebarShadow.style.display = 'none';
}

window.onclick = function (event) {
    if (event.target == document.getElementById('sidebar-shadow')) sidebar_close()
};

function karomandSearch() {
    const search = document.getElementById('search');
    const container = document.getElementById('karomands-container');
    var xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = function () {
        if (this.readyState = 4 && this.status == 200) {
            if (this.responseText != '') {
                container.innerHTML = this.responseText;
            }
            else {
                container.innerHTML = 'Nothing to show';
            }
        }
    };
    xmlHttp.open('GET', 'karomands-search.php?q=' + search.value);
    xmlHttp.send();
}